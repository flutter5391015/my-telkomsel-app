class ActivePeriod {
  int? id;
  String? name;

  ActivePeriod({this.id, this.name});

  ActivePeriod.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}
