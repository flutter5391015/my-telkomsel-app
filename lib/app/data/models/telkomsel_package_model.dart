import 'package:my_telkomsel_app/app/data/models/active_periode_model.dart';
import 'package:my_telkomsel_app/app/data/models/category_model.dart';

class TelkomselPackage {
  Category? category;
  int? price;
  int? priceAfterDiscount;
  ActivePeriod? activePeriod;
  bool? isFree;
  String? packageQuota;

  TelkomselPackage({
    this.category,
    this.price,
    this.priceAfterDiscount,
    this.activePeriod,
    this.isFree,
    this.packageQuota,
  });

  TelkomselPackage.fromJson(Map<String, dynamic> json) {
    category =
        json['category'] != null ? Category?.fromJson(json['category']) : null;
    price = json['price'];
    priceAfterDiscount = json['price_after_discount'];
    activePeriod = json['active_period'] != null
        ? ActivePeriod?.fromJson(json['active_period'])
        : null;
    isFree = json['is_free'];
    packageQuota = json['package_quota'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    if (category != null) {
      data['category'] = category?.toJson();
    }
    data['price'] = price;
    data['price_after_discount'] = priceAfterDiscount;
    if (activePeriod != null) {
      data['active_period'] = activePeriod?.toJson();
    }
    data['is_free'] = isFree;
    data['package_quota'] = packageQuota;
    return data;
  }
}
