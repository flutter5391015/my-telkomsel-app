import 'package:get/get.dart';

import '../controllers/entertainment_category_controller.dart';

class EntertainmentCategoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EntertainmentCategoryController>(
      () => EntertainmentCategoryController(),
    );
  }
}
