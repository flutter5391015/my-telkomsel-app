import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/entertainment_category_controller.dart';

class EntertainmentCategoryView
    extends GetView<EntertainmentCategoryController> {
  const EntertainmentCategoryView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('EntertainmentCategoryView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'EntertainmentCategoryView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
