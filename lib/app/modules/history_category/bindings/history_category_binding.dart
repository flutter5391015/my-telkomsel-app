import 'package:get/get.dart';

import '../controllers/history_category_controller.dart';

class HistoryCategoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HistoryCategoryController>(
      () => HistoryCategoryController(),
    );
  }
}
