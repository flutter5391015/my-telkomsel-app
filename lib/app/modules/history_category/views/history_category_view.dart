import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/history_category_controller.dart';

class HistoryCategoryView extends GetView<HistoryCategoryController> {
  const HistoryCategoryView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HistoryCategoryView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'HistoryCategoryView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
