import 'package:get/get.dart';

import 'package:my_telkomsel_app/app/modules/home/controllers/category_package_controller.dart';
import 'package:my_telkomsel_app/app/modules/home/controllers/home_controller.dart';
import 'package:my_telkomsel_app/app/modules/home/controllers/navigation_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<NavigationController>(
      () => NavigationController(),
    );
    Get.lazyPut<CategoryPackageController>(
      () => CategoryPackageController(),
    );
    Get.lazyPut<HomeController>(
      () => HomeController(),
    );
  }
}
