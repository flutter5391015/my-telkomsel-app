class CategoryPackage {
  String? name;
  String? image;
  String? router;

  CategoryPackage({
    this.name,
    this.image,
    this.router,
  });

  CategoryPackage.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    image = json['image'];
    router = json['router'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['image'] = image;
    data['router'] = router;
    return data;
  }
}
