import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/home/category_package_model.dart';

class CategoryPackageController extends GetxController {
  final myCategoryPackage = <CategoryPackage>[].obs;

  @override
  void onInit() async {
    super.onInit();
    final data =
        await rootBundle.loadString('assets/models/category-package.json');
    final List<dynamic> listData = jsonDecode(data);

    for (var element in listData) {
      myCategoryPackage.add(
        CategoryPackage.fromJson(element),
      );
    }
  }
}
