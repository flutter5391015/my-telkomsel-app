import 'package:get/get.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:my_telkomsel_app/app/modules/login/user_model.dart';

class HomeController extends GetxController {
  final userLogin = User().obs;

  @override
  void onInit() async {
    await initializeDateFormatting(); // ini untuk menginisiasi local
    super.onInit();
  }

  void setUserLogin({required User user}) {
    userLogin.update((value) {
      final data = value!;
      data.name = user.name;
      data.phoneNumber = user.phoneNumber;
      data.remainingQuota = user.remainingQuota;
      data.internetQuota = user.internetQuota;
      data.point = user.point;
      data.smsQuota = user.smsQuota;
      data.phoneQuota = user.phoneQuota;
      data.isVerified = user.isVerified;
      data.activeUntilDate = user.activeUntilDate;
    });
  }

  void setVerified({required bool isVerified}) {
    userLogin.update((value) {
      final data = value!;
      data.isVerified = isVerified;
    });
  }

  String setIndonesiaCurrency({required int value}) {
    final formatCurrency = NumberFormat.currency(
      locale: 'id_ID',
      symbol: 'Rp',
      decimalDigits: 0,
    );
    return formatCurrency.format(value);
  }

  String setDateFormat({required String value}) {
    final DateTime originalDate = DateTime.parse(value);
    return DateFormat('d MMMM y', 'id_ID').format(originalDate);
  }
}
