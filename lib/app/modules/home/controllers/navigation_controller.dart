import 'package:get/get.dart';

class NavigationController extends GetxController {
  final index = 0.obs;
  void setIndex(newIdx) => index.value = newIdx;
}
