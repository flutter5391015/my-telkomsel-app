import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/molecules/bottom_navigation.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/molecules/category_package.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/molecules/covid_awareness.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/molecules/header.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/molecules/news_from_telkomsel.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/molecules/linkaja.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/molecules/special_offer.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/molecules/vourcher.dart';
import 'package:my_telkomsel_app/theme.dart';

import 'package:my_telkomsel_app/app/modules/home/controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: RichText(
          maxLines: 1,
          overflow: TextOverflow.fade,
          softWrap: false,
          text: TextSpan(
            text: 'Hai, ',
            style: Theme.of(context).textTheme.titleSmall!.copyWith(
                  fontSize: 20,
                  height: 24 / 20,
                  fontWeight: FontWeight.w500,
                  color: MyAppColor.WHITE,
                ),
            children: [
              TextSpan(
                text: GetUtils.capitalize(
                  controller.userLogin.value.name!,
                ),
                style: Theme.of(context).textTheme.titleSmall!.copyWith(
                      fontSize: 20,
                      height: 24 / 20,
                      fontWeight: FontWeight.w700,
                      color: MyAppColor.WHITE,
                    ),
              ),
            ],
          ),
        ),
        actions: [
          GestureDetector(
            onTap: () {},
            child: Padding(
              padding: const EdgeInsets.only(right: 16),
              child: SvgPicture.asset(
                'assets/images/icon/qrcode.svg',
              ),
            ),
          )
        ],
        backgroundColor: MyAppColor.RED,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Header(),
            const SizedBox(
              height: 10,
            ),
            Container(
              height: 8,
              color: MyAppColor.GREY,
            ),
            const SizedBox(
              height: 10,
            ),
            const CategoryPackage(),
            const SizedBox(
              height: 10,
            ),
            const NewsFromTelkomsel(),
            const SizedBox(
              height: 25,
            ),
            const CovidAwareness(),
            const SizedBox(
              height: 25,
            ),
            const LinkAja(),
            const SizedBox(
              height: 25,
            ),
            const Voucher(),
            const SizedBox(
              height: 25,
            ),
            const SpecialOffer(),
            const SizedBox(
              height: 32,
            )
          ],
        ),
      ),
      bottomNavigationBar: const BottomNavigation(),
    );
  }
}
