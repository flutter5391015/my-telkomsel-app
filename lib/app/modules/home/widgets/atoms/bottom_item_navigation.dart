import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:my_telkomsel_app/theme.dart';

class BottomItemNavigation extends StatelessWidget {
  final void Function() onTap;
  final String image;
  final String label;
  final bool isActive;
  const BottomItemNavigation({
    super.key,
    required this.onTap,
    required this.image,
    required this.label,
    required this.isActive,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: onTap,
      child: SizedBox(
        height: 49,
        width: 60,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 22,
              width: 22,
              child: SvgPicture.asset(
                image,
                fit: BoxFit.contain,
              ),
            ),
            Text(
              label,
              style: Theme.of(context).textTheme.labelSmall!.copyWith(
                    fontSize: 10,
                    color: isActive ? MyAppColor.RED : MyAppColor.GREY_DARK,
                    height: 12.72 / 10,
                  ),
            )
          ],
        ),
      ),
    );
  }
}
