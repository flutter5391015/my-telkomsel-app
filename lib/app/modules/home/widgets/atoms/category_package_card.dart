import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:my_telkomsel_app/theme.dart';

class CategoryPackageCard extends StatelessWidget {
  final String icon;
  final String name;
  final void Function() onTap;

  const CategoryPackageCard({
    super.key,
    required this.icon,
    required this.name,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 53,
            width: 53,
            decoration: BoxDecoration(
              color: MyAppColor.YELLOW.withOpacity(.1),
              shape: BoxShape.circle,
            ),
            child: SvgPicture.asset(
              'assets/images/icon/$icon',
              width: 32,
              height: 32,
              fit: BoxFit.scaleDown,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            name,
            style: Theme.of(context).textTheme.labelMedium!.copyWith(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  height: 20 / 14,
                ),
          ),
        ],
      ),
    );
  }
}
