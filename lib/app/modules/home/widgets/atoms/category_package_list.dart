import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/home/controllers/category_package_controller.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/category_package_card.dart';

class CategoryPackageList extends StatelessWidget {
  const CategoryPackageList({super.key});

  @override
  Widget build(BuildContext context) {
    final categoryPackageController = Get.find<CategoryPackageController>();

    return Obx(
      () => SizedBox(
        height: 210,
        child: GridView.count(
          scrollDirection: Axis.vertical,
          physics: const NeverScrollableScrollPhysics(),
          childAspectRatio: 75 / 92,
          mainAxisSpacing: 0,
          crossAxisCount: 4,
          children: categoryPackageController.myCategoryPackage
              .map(
                (e) => CategoryPackageCard(
                  icon: e.image.toString(),
                  name: e.name.toString(),
                  onTap: () => Get.toNamed(e.router.toString()),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
