import 'package:flutter/material.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/covid_awareness_card.dart';

class CovidAwarenessList extends StatelessWidget {
  const CovidAwarenessList({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      child: ListView(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
        ),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: const [
          SizedBox(
            width: 20,
          ),
          CovidAwarenessCard(
            image: 'assets/images/covid_awareness/discount-25.png',
            bodyText: 'Diskon Spesial 25% Untuk Pelanggan Baru',
          ),
          CovidAwarenessCard(
            image: 'assets/images/covid_awareness/halodoc.png',
            bodyText: 'Bebas Kuota Akses Layanan Kesehatan',
          ),
          CovidAwarenessCard(
            image: 'assets/images/covid_awareness/telkomsel-tanggap.png',
            bodyText: 'Telkomsel #TerusBergerakMaju ...',
          ),
          CovidAwarenessCard(
            image: 'assets/images/covid_awareness/dirumahaja.png',
            bodyText: '#DiRumahTerusProduktif',
          ),
        ],
      ),
    );
  }
}
