import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/theme.dart';

class LinkAjaCard extends StatelessWidget {
  final String image;
  final String bodyText;

  const LinkAjaCard({
    super.key,
    required this.image,
    required this.bodyText,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 20),
      height: 200,
      width: 145,
      clipBehavior: Clip.hardEdge,
      decoration: const ShapeDecoration(
        shadows: [
          BoxShadow(
            color: Color.fromRGBO(16, 37, 53, 0.08),
            blurRadius: 16,
            offset: Offset(0, 2),
          )
        ],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(4),
          ),
        ),
        color: MyAppColor.WHITE,
      ),
      child: Column(
        children: [
          SizedBox(
            height: 96,
            width: Get.width,
            child: Image.asset(
              image,
              fit: BoxFit.cover,
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
            child: Text(
              bodyText,
              style: Theme.of(context).textTheme.labelMedium!.copyWith(
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                    height: 20 / 13,
                  ),
            ),
          )
        ],
      ),
    );
  }
}
