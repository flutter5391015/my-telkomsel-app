import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/linkaja_card.dart';

class ItemLinkAja extends StatelessWidget {
  const ItemLinkAja({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 160,
      width: Get.width,
      child: ListView(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
        ),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: const [
          SizedBox(
            width: 20,
          ),
          LinkAjaCard(
            bodyText: 'Bayar Serba Cepat',
            image: 'assets/images/linkaja/linkaja.png',
          ),
          LinkAjaCard(
            bodyText: 'Cukup Snap QR',
            image: 'assets/images/linkaja/snap-qr.png',
          ),
          LinkAjaCard(
            bodyText: 'NO! Credit Card',
            image: 'assets/images/linkaja/no-cc.png',
          ),
        ],
      ),
    );
  }
}
