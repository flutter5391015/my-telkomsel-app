

import 'package:flutter/material.dart';

class NewsFromTelkomselImage extends StatelessWidget {
  final String image;
  const NewsFromTelkomselImage({super.key, required this.image});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 248,
      height: 100,
      margin: const EdgeInsets.only(right: 20),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(image),
        ),
      ),
    );
  }
}
