import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/news_from_telkomsel_image.dart';

class NewsFromTelkomselList extends StatelessWidget {
  const NewsFromTelkomselList({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100,
      width: Get.width,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: const [
          SizedBox(
            width: 20,
          ),
          NewsFromTelkomselImage(
            image: 'assets/images/advertisement/internet-omg.png',
          ),
          NewsFromTelkomselImage(
            image: 'assets/images/advertisement/undian-ketengan.png',
          ),
        ],
      ),
    );
  }
}
