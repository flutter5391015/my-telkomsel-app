import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/theme.dart';

class SpecialOfferCard extends StatelessWidget {
  final String image;
  final String bodyText;
  const SpecialOfferCard({
    super.key,
    required this.image,
    required this.bodyText,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 20),
      width: 248,
      height: 178,
      clipBehavior: Clip.hardEdge,
      decoration: const ShapeDecoration(
        shadows: [
          BoxShadow(
            color: Color.fromRGBO(16, 37, 53, 0.08),
            offset: Offset(0, 2),
            blurRadius: 16,
          )
        ],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(4),
          ),
        ),
        color: MyAppColor.WHITE,
      ),
      child: Column(
        children: [
          SizedBox(
            height: 112,
            width: Get.width,
            child: Image.asset(
              image,
              fit: BoxFit.cover,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(12, 12, 12, 12),
            child: Text(
              bodyText,
              style: Theme.of(context).textTheme.labelMedium!.copyWith(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    height: 17.81 / 14,
                  ),
            ),
          )
        ],
      ),
    );
  }
}
