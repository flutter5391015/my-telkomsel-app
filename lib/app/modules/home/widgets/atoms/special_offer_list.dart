import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/special_offer_card.dart';

class SpecialOfferList extends StatelessWidget {
  const SpecialOfferList({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 205,
      width: Get.width,
      child: ListView(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
        ),
        scrollDirection: Axis.horizontal,
        children: const [
          SizedBox(
            width: 20,
          ),
          SpecialOfferCard(
            bodyText: 'Terus Belajar dari Rumahmu dengan Ruangguru!',
            image: 'assets/images/special_offer/ruang-guru.png',
          ),
          SpecialOfferCard(
            bodyText: 'Belajar Kini Makin Mudah dengan Paket ilmupedia!',
            image: 'assets/images/special_offer/ilmupedia.png',
          ),
        ],
      ),
    );
  }
}
