// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/theme.dart';

enum MyPackage { INTERNET, TELPON, SMS }

class SummaryPackageCard extends StatelessWidget {
  final String amount;
  final MyPackage type;

  const SummaryPackageCard({
    required this.amount,
    required this.type,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      shadowColor: const Color.fromRGBO(16, 37, 53, 0.08),
      color: MyAppColor.WHITE,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4),
      ),
      child: Container(
        width: 115,
        height: 68,
        padding: const EdgeInsets.fromLTRB(10, 8, 12, 8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              GetUtils.capitalizeFirst(type.name).toString(),
              style: Theme.of(context).textTheme.labelMedium!.copyWith(
                    fontWeight: FontWeight.w400,
                    fontSize: 13,
                    height: 17 / 13,
                  ),
            ),
            const SizedBox(
              height: 4,
            ),
            RichText(
              text: TextSpan(
                text: '$amount ',
                style: Theme.of(context).textTheme.labelMedium!.copyWith(
                      fontWeight: FontWeight.w700,
                      fontSize: 24,
                      height: 31 / 24,
                      color: MyAppColor.RED,
                    ),
                children: [
                  smallTitle(
                    package: type,
                    context: context,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  TextSpan smallTitle(
      {required MyPackage package, required BuildContext context}) {
    if (package == MyPackage.INTERNET) {
      return TextSpan(
        text: 'GB',
        style: Theme.of(context).textTheme.labelMedium!.copyWith(
              fontWeight: FontWeight.w400,
              fontSize: 14,
              height: 17.81 / 14,
              color: MyAppColor.GREY_DARK,
            ),
      );
    } else if (package == MyPackage.SMS) {
      return TextSpan(
        text: 'SMS',
        style: Theme.of(context).textTheme.labelMedium!.copyWith(
              fontWeight: FontWeight.w400,
              fontSize: 14,
              height: 17.81 / 14,
              color: MyAppColor.GREY_DARK,
            ),
      );
    } else {
      return TextSpan(
        text: 'Min',
        style: Theme.of(context).textTheme.labelMedium!.copyWith(
              fontWeight: FontWeight.w400,
              fontSize: 14,
              height: 17.81 / 14,
              color: MyAppColor.GREY_DARK,
            ),
      );
    }
  }
}
