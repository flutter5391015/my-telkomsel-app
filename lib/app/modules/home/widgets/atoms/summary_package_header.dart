import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/home/controllers/home_controller.dart';
import 'package:my_telkomsel_app/theme.dart';

class SummaryPackageHeader extends StatelessWidget {
  const SummaryPackageHeader({super.key});

  @override
  Widget build(BuildContext context) {
    final homeController = Get.find<HomeController>();
    return Obx(
      () => ClipPath(
        clipper: ClipCardClipper(),
        child: Container(
          width: Get.width,
          margin: const EdgeInsets.symmetric(
            horizontal: 20,
          ),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
            gradient: MyAppColor.LINER_RED,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  left: 16,
                  top: 16,
                  right: 16,
                  bottom: 5,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          homeController.userLogin.value.phoneNumber.toString(),
                          style:
                              Theme.of(context).textTheme.titleLarge!.copyWith(
                                    color: MyAppColor.WHITE,
                                    fontSize: 18,
                                    height: 18 / 18,
                                    letterSpacing: 2,
                                  ),
                        ),
                        Image.asset(
                          'assets/images/simpati.png',
                          width: 84,
                          height: 15,
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Text(
                      'Sisa Pulsa Anda',
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color: MyAppColor.WHITE,
                            fontSize: 14,
                            height: 17.81 / 18,
                            fontWeight: FontWeight.w500,
                          ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          homeController.setIndonesiaCurrency(
                              value: homeController
                                  .userLogin.value.remainingQuota!),
                          style:
                              Theme.of(context).textTheme.titleLarge!.copyWith(
                                    color: MyAppColor.WHITE,
                                    fontSize: 24,
                                    height: 32 / 24,
                                  ),
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: MyAppColor.YELLOW,
                            shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(4),
                              ),
                            ),
                          ),
                          onPressed: () {},
                          child: Text(
                            'Isi Pulsa',
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium!
                                .copyWith(
                                  color: MyAppColor.BLACK,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  height: 18 / 14,
                                ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              Divider(
                color: MyAppColor.BLACK.withOpacity(0.1),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 5,
                  left: 16,
                  right: 16,
                  bottom: 15,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RichText(
                      text: TextSpan(
                        text: 'Berlaku sampai ',
                        style: Theme.of(context).textTheme.labelSmall!.copyWith(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              height: 17.81 / 14,
                              color: MyAppColor.WHITE,
                            ),
                        children: [
                          TextSpan(
                            text: homeController.setDateFormat(
                                value: homeController
                                    .userLogin.value.activeUntilDate!),
                            style: Theme.of(context)
                                .textTheme
                                .labelSmall!
                                .copyWith(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700,
                                  height: 17.81 / 14,
                                  color: MyAppColor.WHITE,
                                ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Text(
                          'Telkomsel POIN',
                          style:
                              Theme.of(context).textTheme.labelSmall!.copyWith(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    height: 17.81 / 14,
                                    color: MyAppColor.WHITE,
                                  ),
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(horizontal: 4),
                          decoration: BoxDecoration(
                            color: MyAppColor.YELLOW,
                            borderRadius: BorderRadius.circular(4),
                          ),
                          child: Text(
                            homeController.userLogin.value.point.toString(),
                            style: Theme.of(context)
                                .textTheme
                                .labelSmall!
                                .copyWith(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700,
                                  height: 17.81 / 14,
                                  color: MyAppColor.BLACK,
                                ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ClipCardClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    /*Path path = Path();
    path.lineTo(0, size.height);
    path.lineTo(size.width - 80, size.height);
    path.lineTo(size.width, size.height - 80);
    path.lineTo(size.width, 0);*/

    Path path_0 = Path();
    path_0.moveTo(size.width * 0.02388060, 0);
    path_0.lineTo(size.width * 0.9761194, 0);
    path_0.cubicTo(size.width * 0.9893075, 0, size.width,
        size.height * 0.01915369, size.width, size.height * 0.04278086);
    path_0.lineTo(size.width, size.height * 0.7892139);
    path_0.cubicTo(
        size.width,
        size.height * 0.8004332,
        size.width * 0.9975403,
        size.height * 0.8112086,
        size.width * 0.9931493,
        size.height * 0.8192086);
    path_0.lineTo(size.width * 0.9009582, size.height * 0.9872086);
    path_0.cubicTo(
        size.width * 0.8964687,
        size.height * 0.9953904,
        size.width * 0.8903313,
        size.height,
        size.width * 0.8839284,
        size.height);
    path_0.lineTo(size.width * 0.02388060, size.height);
    path_0.cubicTo(size.width * 0.01069170, size.height, 0,
        size.height * 0.9808449, 0, size.height * 0.9572193);
    path_0.lineTo(0, size.height * 0.04278075);
    path_0.cubicTo(0, size.height * 0.01915364, size.width * 0.01069170, 0,
        size.width * 0.02388060, 0);
    path_0.close();

    return path_0;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
