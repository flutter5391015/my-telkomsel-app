import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/home/controllers/home_controller.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/summary_package_card.dart';

class SummaryPackageList extends StatelessWidget {
  const SummaryPackageList({super.key});

  @override
  Widget build(BuildContext context) {
    final homeController = Get.find<HomeController>();
    return Obx(
      () => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SummaryPackageCard(
            type: MyPackage.INTERNET,
            amount: homeController.userLogin.value.internetQuota.toString(),
          ),
          SummaryPackageCard(
            type: MyPackage.TELPON,
            amount: homeController.userLogin.value.phoneQuota.toString(),
          ),
          SummaryPackageCard(
            type: MyPackage.SMS,
            amount: homeController.userLogin.value.smsQuota.toString(),
          ),
        ],
      ),
    );
  }
}
