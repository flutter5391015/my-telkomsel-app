import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/voucher_card.dart';

class VoucherList extends StatelessWidget {
  const VoucherList({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 205,
      width: Get.width,
      child: ListView(
        padding: const EdgeInsets.symmetric(
          vertical: 10,
        ),
        scrollDirection: Axis.horizontal,
        children: const [
          SizedBox(
            width: 20,
          ),
          VoucherCard(
            bodyText: 'Double Benefits from DOUBLE UNTUNG',
            image: 'assets/images/voucher/double-untung.png',
          ),
          VoucherCard(
            bodyText: 'Join Undi-Undi Hepi 2020!',
            image: 'assets/images/voucher/undian.png',
          ),
          VoucherCard(
            bodyText: 'Get Samsung Galaxy S20 Series with Best kartuHalo ...',
            image: 'assets/images/voucher/telkomsel-s20.png',
          ),
          VoucherCard(
            bodyText: 'Mau Dapetin Konten Favorit? Ke Outlet Pulsa aja!',
            image: 'assets/images/voucher/paket-digital.png',
          ),
        ],
      ),
    );
  }
}
