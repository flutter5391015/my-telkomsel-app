import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/home/controllers/navigation_controller.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/bottom_item_navigation.dart';
import 'package:my_telkomsel_app/theme.dart';

class BottomNavigation extends StatelessWidget {
  const BottomNavigation({super.key});

  @override
  Widget build(BuildContext context) {
    final navigationController = Get.find<NavigationController>();
    return Obx(
      () => BottomAppBar(
        height: 100,
        color: Colors.red,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            BottomItemNavigation(
              image: navigationController.index.value == 0
                  ? 'assets/images/icon/beranda-active.svg'
                  : 'assets/images/icon/beranda.svg',
              label: 'Beranda',
              onTap: () => navigationController.setIndex(0),
              isActive: navigationController.index.value == 0,
            ),
            BottomItemNavigation(
              image: navigationController.index.value == 1
                  ? 'assets/images/icon/Ihistory-active.svg'
                  : 'assets/images/icon/Ihistory.svg',
              label: 'Riwayat',
              onTap: () => navigationController.setIndex(1),
              isActive: navigationController.index.value == 1,
            ),
            BottomItemNavigation(
              image: navigationController.index.value == 2
                  ? 'assets/images/icon/help-active.svg'
                  : 'assets/images/icon/help.svg',
              label: 'Bantuan',
              onTap: () => navigationController.setIndex(2),
              isActive: navigationController.index.value == 2,
            ),
            BottomItemNavigation(
              image: navigationController.index.value == 3
                  ? 'assets/images/icon/inbox-active.svg'
                  : 'assets/images/icon/inbox.svg',
              label: 'Inbox',
              onTap: () => navigationController.setIndex(3),
              isActive: navigationController.index.value == 3,
            ),
            BottomItemNavigation(
              image: navigationController.index.value == 4
                  ? 'assets/images/icon/myprofile-active.svg'
                  : 'assets/images/icon/myprofile.svg',
              label: 'Akun saya',
              onTap: () => navigationController.setIndex(4),
              isActive: navigationController.index.value == 4,
            ),
          ],
        ),
      ),
    );
  }
}
