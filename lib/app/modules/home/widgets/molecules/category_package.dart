import 'package:flutter/material.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/category_package_list.dart';
import 'package:my_telkomsel_app/app/widgets/section_title.dart';

class CategoryPackage extends StatelessWidget {
  const CategoryPackage({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: const [
          SectionTitle(
            title: 'Kategori Paket',
          ),
          SizedBox(
            height: 5,
          ),
          CategoryPackageList(),
        ],
      ),
    );
  }
}
