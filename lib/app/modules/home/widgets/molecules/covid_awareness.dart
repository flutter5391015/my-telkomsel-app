import 'package:flutter/material.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/covid_awareness_list.dart';
import 'package:my_telkomsel_app/app/widgets/section_title.dart';
import 'package:my_telkomsel_app/app/widgets/show_all_label.dart';

class CovidAwareness extends StatelessWidget {
  const CovidAwareness({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              SectionTitle(
                title: 'Tanggapan COVID-19',
              ),
              ShowAllLabel(),
            ],
          ),
        ),
        const SizedBox(
          height: 30,
        ),
        const CovidAwarenessList(),
      ],
    );
  }
}
