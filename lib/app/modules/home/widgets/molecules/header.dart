import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/summary_package_header.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/summary_package_list.dart';

class Header extends StatelessWidget {
  const Header({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          height: 250,
          width: Get.width,
          child: SvgPicture.asset(
            'assets/images/icon/background-header.svg',
            fit: BoxFit.cover,
          ),
        ),
        Column(
          children: const [
            SummaryPackageHeader(),
            SizedBox(
              height: 10,
            ),
            SummaryPackageList(),
            SizedBox(
              height: 20,
            ),
          ],
        )
      ],
    );
  }
}
