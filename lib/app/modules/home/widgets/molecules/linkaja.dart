import 'package:flutter/material.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/linkaja_list.dart';
import 'package:my_telkomsel_app/app/widgets/section_title.dart';

class LinkAja extends StatelessWidget {
  const LinkAja({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SectionTitle(
                title: 'AYO! Pake LinkAja',
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                'Pakai LinkAja untuk beli pulsa, beli paket data dan bayar tagihan lebih mudah.',
                style: Theme.of(context).textTheme.labelSmall!.copyWith(
                      fontSize: 12,
                      height: 20 / 12,
                    ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 30,
        ),
        const ItemLinkAja(),
      ],
    );
  }
}
