import 'package:flutter/material.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/news_from_telkomsel_list.dart';
import 'package:my_telkomsel_app/app/widgets/section_title.dart';
import 'package:my_telkomsel_app/app/widgets/show_all_label.dart';

class NewsFromTelkomsel extends StatelessWidget {
  const NewsFromTelkomsel({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              SectionTitle(
                title: 'Terbaru dari Telkomsel',
              ),
              ShowAllLabel(),
            ],
          ),
        ),
        const SizedBox(
          height: 30,
        ),
        const NewsFromTelkomselList(),
      ],
    );
  }
}
