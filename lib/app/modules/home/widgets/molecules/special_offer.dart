import 'package:flutter/material.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/atoms/special_offer_list.dart';
import 'package:my_telkomsel_app/app/widgets/section_title.dart';
import 'package:my_telkomsel_app/app/widgets/show_all_label.dart';

class SpecialOffer extends StatelessWidget {
  const SpecialOffer({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              SectionTitle(
                title: 'Penawaran Khusus',
              ),
              ShowAllLabel(),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        const SpecialOfferList(),
      ],
    );
  }
}
