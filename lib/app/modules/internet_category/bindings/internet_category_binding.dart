import 'package:get/get.dart';

import '../controllers/internet_category_controller.dart';

class InternetCategoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<InternetCategoryController>(
      () => InternetCategoryController(),
    );
  }
}
