import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/data/models/category_model.dart';
import 'package:my_telkomsel_app/app/data/models/telkomsel_package_model.dart';

class InternetCategoryController extends GetxController {
  final internetSubscribeList = <TelkomselPackage>[].obs;
  final categoryList = <Category>[].obs;
  final belajarDirumahList = <TelkomselPackage>[].obs;
  final popularList = <TelkomselPackage>[].obs;

  @override
  void onInit() {
    super.onInit();
    fetchDataTelkomselSubscribe();
    fetchDataCategoryPackage();
    fetchDataBelajarDirumah();
    fetchDataPopular();
  }

  Future<void> fetchDataTelkomselSubscribe() async {
    final data = await rootBundle
        .loadString('assets/models/telkomsel-subscribe-package.json');
    final List<dynamic> listData = jsonDecode(data);

    for (var element in listData) {
      internetSubscribeList.add(
        TelkomselPackage.fromJson(element),
      );
    }
  }

  Future<void> fetchDataPopular() async {
    final data = await rootBundle.loadString('assets/models/popular.json');
    final List<dynamic> listData = jsonDecode(data);

    for (var element in listData) {
      popularList.add(
        TelkomselPackage.fromJson(element),
      );
    }
  }

  Future<void> fetchDataBelajarDirumah() async {
    final data =
        await rootBundle.loadString('assets/models/belajar-dirumah.json');
    final List<dynamic> listData = jsonDecode(data);

    for (var element in listData) {
      belajarDirumahList.add(
        TelkomselPackage.fromJson(element),
      );
    }
  }

  Future<void> fetchDataCategoryPackage() async {
    final data = await rootBundle.loadString('assets/models/category.json');
    final List<dynamic> listData = jsonDecode(data);

    for (var element in listData) {
      categoryList.add(
        Category.fromJson(element),
      );
    }
  }

  int getTotalData({
    required String type,
  }) {
    if (type == 'Langganan Kamu') {
      return internetSubscribeList.length;
    } else if (type == 'Popular') {
      return popularList.length;
    } else {
      return belajarDirumahList.length;
    }
  }

  List<TelkomselPackage> getSelectedData({
    required String type,
  }) {
    if (type == 'Langganan Kamu') {
      return internetSubscribeList;
    } else if (type == 'Popular') {
      return popularList;
    } else {
      return belajarDirumahList;
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}
