import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/home/widgets/molecules/news_from_telkomsel.dart';

import 'package:my_telkomsel_app/app/modules/internet_category/controllers/internet_category_controller.dart';
import 'package:my_telkomsel_app/app/modules/internet_category/widgets/molecules/category_package.dart';
import 'package:my_telkomsel_app/app/modules/internet_category/widgets/molecules/input_search.dart';
import 'package:my_telkomsel_app/app/modules/internet_category/widgets/molecules/internet_package.dart';
import 'package:my_telkomsel_app/app/modules/search/controllers/telkomsel_search_controller.dart';
import 'package:my_telkomsel_app/app/routes/app_pages.dart';

import 'package:my_telkomsel_app/theme.dart';

class InternetCategoryView extends GetView<InternetCategoryController> {
  const InternetCategoryView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final searchController = Get.put(
      TelkomselSearchController(),
      permanent: true,
    );

    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Get.offAllNamed(Routes.HOME);
          },
          child: const Icon(Icons.arrow_back_ios),
        ),
        title: Text(
          'Paket Internet',
          style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                fontSize: 18,
                height: 24 / 18,
                fontWeight: FontWeight.w600,
              ),
        ),
        centerTitle: true,
        backgroundColor: MyAppColor.WHITE,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            GestureDetector(
              onTap: () {
                searchController.showSearchView();
              },
              child: const InputSearch(),
            ),
            const NewsFromTelkomsel(),
            const SizedBox(
              height: 32,
            ),
            const InternetPackage(
              titleSection: 'Langganan Kamu',
            ),
            const SizedBox(
              height: 32,
            ),
            const CategoryPackage(),
            const SizedBox(
              height: 32,
            ),
            const InternetPackage(
              titleSection: 'Popular',
            ),
            const SizedBox(
              height: 32,
            ),
            const InternetPackage(
              titleSection: 'Belajar #dirumahaja',
            ),
          ],
        ),
      ),
    );
  }
}
