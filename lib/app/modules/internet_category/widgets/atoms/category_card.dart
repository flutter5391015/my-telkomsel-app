import 'package:flutter/material.dart';
import 'package:my_telkomsel_app/theme.dart';

class CategoryCard extends StatelessWidget {
  final int index;
  final int totalData;
  final String title;
  final void Function() onTap;

  const CategoryCard({
    super.key,
    required this.index,
    required this.totalData,
    required this.title,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: 139,
        height: 60,
        padding: const EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 10,
        ),
        decoration: const BoxDecoration(
          gradient: MyAppColor.LINER_RED,
          borderRadius: BorderRadius.all(
            Radius.circular(4),
          ),
        ),
        child: Text(
          title,
          style: Theme.of(context).textTheme.titleMedium!.copyWith(
                fontSize: 14,
                height: 20 / 14,
                color: MyAppColor.WHITE,
              ),
        ),
      ),
    );
  }
}
