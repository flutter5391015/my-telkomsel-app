import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/data/models/telkomsel_package_model.dart';
import 'package:my_telkomsel_app/app/modules/home/controllers/home_controller.dart';
import 'package:my_telkomsel_app/theme.dart';

class InternetPackageCard extends StatelessWidget {
  final int index;
  final TelkomselPackage data;
  const InternetPackageCard(
      {super.key, required this.index, required this.data});

  @override
  Widget build(BuildContext context) {
    final homeController = Get.find<HomeController>();

    return Container(
      margin: index == 0
          ? const EdgeInsets.only(
              left: 20,
              right: 20,
            )
          : const EdgeInsets.only(
              right: 20,
            ),
      padding: const EdgeInsets.all(
        12,
      ),
      width: 248,
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: MyAppColor.GREY,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(4),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                data.packageQuota.toString(),
                style: Theme.of(context).textTheme.titleLarge!.copyWith(
                      fontSize: 24,
                      height: 32 / 24,
                    ),
              ),
              Container(
                height: 28,
                width: 90,
                padding: const EdgeInsets.symmetric(
                  horizontal: 8,
                ),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(16),
                  ),
                  color: MyAppColor.GREY,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 16,
                      width: 16,
                      child: SvgPicture.asset(
                        'assets/images/icon/timer-sand.svg',
                        fit: BoxFit.cover,
                      ),
                    ),
                    Text(
                      data.activePeriod!.name.toString().toUpperCase(),
                      style: Theme.of(context).textTheme.titleLarge!.copyWith(
                            fontSize: 12,
                            height: 16 / 12,
                          ),
                    ),
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {},
                child: SizedBox(
                  height: 24,
                  width: 24,
                  child:
                      SvgPicture.asset('assets/images/icon/bookmark-fill.svg'),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Text(
            data.priceAfterDiscount != 0
                ? homeController.setIndonesiaCurrency(
                    value: data.price!,
                  )
                : '',
            style: Theme.of(context).textTheme.titleSmall!.copyWith(
                  fontWeight: FontWeight.w400,
                  color: MyAppColor.GREY_DARK,
                  fontSize: 12,
                  height: 16 / 12,
                  decoration: TextDecoration.lineThrough,
                ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              if (!data.isFree!)
                Text(
                  homeController.setIndonesiaCurrency(
                    value: (data.priceAfterDiscount! > 0
                        ? data.priceAfterDiscount!
                        : data.price!),
                  ),
                  style: Theme.of(context).textTheme.titleLarge!.copyWith(
                        fontWeight: FontWeight.w400,
                        color: MyAppColor.RED,
                        fontSize: 18,
                        height: 24 / 18,
                      ),
                ),
              if (data.isFree!)
                Text(
                  'Free',
                  style: Theme.of(context).textTheme.titleLarge!.copyWith(
                        fontWeight: FontWeight.w400,
                        color: MyAppColor.RED,
                        fontSize: 18,
                        height: 24 / 18,
                      ),
                ),
              Text(
                data.category!.name.toString(),
                style: Theme.of(context).textTheme.titleMedium!.copyWith(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      height: 20 / 14,
                    ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
