import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/internet_category/controllers/internet_category_controller.dart';
import 'package:my_telkomsel_app/app/modules/internet_category/widgets/atoms/category_card.dart';
import 'package:my_telkomsel_app/app/widgets/section_title.dart';
import 'package:my_telkomsel_app/app/widgets/show_all_label.dart';
import 'package:my_telkomsel_app/theme.dart';

class CategoryPackage extends StatelessWidget {
  const CategoryPackage({super.key});

  @override
  Widget build(BuildContext context) {
    final internetCategoryController = Get.find<InternetCategoryController>();

    return Obx(() => Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  SectionTitle(title: 'Ketegori'),
                  ShowAllLabel(),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 130,
              child: GridView.builder(
                padding: const EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                itemCount: internetCategoryController.categoryList.length,
                scrollDirection: Axis.horizontal,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 8,
                  crossAxisSpacing: 8,
                  childAspectRatio: 60 / 139,
                ),
                itemBuilder: (context, index) => CategoryCard(
                  index: index,
                  totalData: internetCategoryController.categoryList.length,
                  title: internetCategoryController.categoryList[index].name
                      .toString(),
                  onTap: () => {},
                ),
              ),
            ),
          ],
        ));
  }
}
