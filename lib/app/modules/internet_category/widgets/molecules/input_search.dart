import 'package:flutter/material.dart';
import 'package:my_telkomsel_app/theme.dart';

class InputSearch extends StatelessWidget {
  const InputSearch({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(16, 12, 16, 16),
      child: const TextField(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(
            bottom: 0.0,
            top: 15.0,
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(4),
            ),
          ),
          prefixIcon: Icon(
            Icons.search,
          ),
          disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
            color: MyAppColor.GREY,
          )),
          prefixIconColor: MyAppColor.PLACEHOLDER,
          enabled: false,
          hintText: 'Cari paket favorit kamu ...',
          hintStyle: TextStyle(
            color: MyAppColor.PLACEHOLDER,
          ),
          fillColor: MyAppColor.GREY,
          filled: true,
        ),
      ),
    );
  }
}
