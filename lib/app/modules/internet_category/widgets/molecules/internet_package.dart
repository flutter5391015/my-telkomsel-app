import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/internet_category/controllers/internet_category_controller.dart';
import 'package:my_telkomsel_app/app/modules/internet_category/widgets/atoms/internet_package_card.dart';
import 'package:my_telkomsel_app/theme.dart';

class InternetPackage extends StatelessWidget {
  final String titleSection;
  const InternetPackage({super.key, required this.titleSection});

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<InternetCategoryController>();

    return Obx(() => SizedBox(
          height: 180,
          width: Get.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                ),
                child: Text(
                  titleSection,
                  style: Theme.of(context).textTheme.titleLarge!.copyWith(
                        fontSize: 16,
                        height: 20 / 16,
                      ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 120,
                child: ListView.builder(
                  itemCount: controller.getTotalData(type: titleSection),
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return InternetPackageCard(
                      index: index,
                      data:
                          controller.getSelectedData(type: titleSection)[index],
                    );
                  },
                ),
              )
            ],
          ),
        ));
  }
}
