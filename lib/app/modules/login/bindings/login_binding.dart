import 'package:get/get.dart';

import 'package:my_telkomsel_app/app/modules/login/controllers/form_login_controller.dart';

import '../controllers/login_controller.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FormLoginController>(
      () => FormLoginController(),
    );
    Get.lazyPut<LoginController>(
      () => LoginController(),
    );
  }
}
