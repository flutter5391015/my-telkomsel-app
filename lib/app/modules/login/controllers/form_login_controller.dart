import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FormLoginController extends GetxController {
  final phoneNumber = ''.obs;
  final formLoginKey = GlobalKey<FormState>();
  final _indonesiaFormat = RegExp(r'^(?:\+62|62|0)[2-9][0-9]{7,11}$');

  final isAgreementChecked = false.obs;

  void setPhoneNumber({required String value}) {
    phoneNumber.value = value;
  }

  bool checkingPhoneFormat({required String value}) {
    return _indonesiaFormat.hasMatch(value);
  }

  void setIsAgreement({required bool value}) {
    isAgreementChecked.value = value;
  }
}
