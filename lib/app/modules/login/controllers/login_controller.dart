import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/home/controllers/home_controller.dart';
import 'package:my_telkomsel_app/app/modules/login/controllers/form_login_controller.dart';
import 'package:my_telkomsel_app/app/modules/login/user_model.dart';
import 'package:my_telkomsel_app/app/routes/app_pages.dart';
import 'package:my_telkomsel_app/theme.dart';

class LoginController extends GetxController {
  final isSaving = false.obs;
  final userLogin = User().obs;
  late User _user;
  final formLoginController = Get.find<FormLoginController>();
  final homeController = Get.put(
    HomeController(),
    permanent: true,
  );

  @override
  void onInit() async {
    super.onInit();
    // Baca file JSON dan ubah menjadi objek Dart
    String data = await rootBundle.loadString('assets/models/user.json');
    _user = User.fromJson(
      jsonDecode(data),
    );
  }

  Future<void> saveAndNavigate({
    required GlobalKey<FormState> formKey,
  }) async {
    final formLogin = formKey.currentState!;
    isSaving.value = true;
    if (formLogin.validate()) {
      formLogin.save();

      await Future.delayed(
        const Duration(
          seconds: 2,
        ),
      );

      if (_user.phoneNumber != formLoginController.phoneNumber.value) {
        Get.snackbar(
          "Error",
          "Please check your phone number",
          icon: const Icon(
            Icons.error,
            color: MyAppColor.RED,
          ),
          duration: const Duration(seconds: 5),
          margin: const EdgeInsets.all(20),
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: MyAppColor.RED.withOpacity(0.1),
          forwardAnimationCurve: Curves.easeInBack,
          colorText: MyAppColor.BLACK,
        );

        isSaving.value = false;
        return;
      }

      _user.isVerified = false;
      homeController.setUserLogin(user: _user);

      Get.toNamed(Routes.VERIFICATION);
    }

    isSaving.value = false;
  }
}
