class User {
  String? name;
  String? phoneNumber;
  int? remainingQuota;
  int? point;
  double? internetQuota;
  int? phoneQuota;
  int? smsQuota;
  bool? isVerified;
  String? activeUntilDate;

  User(
      {this.name,
      this.phoneNumber,
      this.remainingQuota,
      this.point,
      this.internetQuota,
      this.phoneQuota,
      this.smsQuota,
      this.activeUntilDate,
      this.isVerified});

  User.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    phoneNumber = json['phone_number'];
    remainingQuota = json['remaining_quota'];
    point = json['point'];
    internetQuota = json['internet_quota'];
    phoneQuota = json['phone_quota'];
    smsQuota = json['sms_quota'];
    activeUntilDate = json['active_until_date'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['phone_number'] = phoneNumber;
    data['remaining_quota'] = remainingQuota;
    data['point'] = point;
    data['internet_quota'] = internetQuota;
    data['phone_quota'] = phoneQuota;
    data['sms_quota'] = smsQuota;
    data['active_until_date'] = activeUntilDate;
    return data;
  }
}
