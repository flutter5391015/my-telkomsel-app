import 'package:flutter/material.dart';

import 'package:get/get.dart';

import 'package:my_telkomsel_app/app/modules/login/controllers/login_controller.dart';
import 'package:my_telkomsel_app/app/modules/login/widgets/molecules/image_login.dart';
import 'package:my_telkomsel_app/app/modules/login/widgets/molecules/form.dart';

import 'package:my_telkomsel_app/theme.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyAppColor.WHITE,
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(
          FocusNode(),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
          ),
          child: ListView(
            children: [
              const Align(
                alignment: Alignment.topLeft,
                child: ImageLogin(),
              ),
              const SizedBox(
                height: 16,
              ),
              Text(
                'Silahkan masuk dengan nomor kartu kamu',
                style: Theme.of(context).textTheme.titleLarge!.copyWith(
                      fontSize: 18,
                      height: 23 / 18,
                    ),
              ),
              const SizedBox(
                height: 24,
              ),
              const FormLogin(),
            ],
          ),
        ),
      ),
    );
  }
}
