import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoginWithSocialMedia extends StatelessWidget {
  const LoginWithSocialMedia({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Theme(
          data: ThemeData(
            outlinedButtonTheme: OutlinedButtonThemeData(
              style: OutlinedButton.styleFrom(
                //splashFactory: NoSplash.splashFactory, untuk menghilangkan efek button di klik
                foregroundColor: const Color(0xFF3B5998),
                side: const BorderSide(
                  color: Color(0xFF3B5998),
                ),
              ),
            ),
          ),
          child: SizedBox(
            height: 42,
            width: 157,
            child: OutlinedButton(
              onPressed: () {},
              child: SvgPicture.asset(
                'assets/images/icon/facebook.svg',
              ),
            ),
          ),
        ),
        Theme(
          data: ThemeData(
            outlinedButtonTheme: OutlinedButtonThemeData(
              style: OutlinedButton.styleFrom(
                side: const BorderSide(
                  color: Color(0xFF1DA1F2),
                ),
              ),
            ),
          ),
          child: SizedBox(
            height: 42,
            width: 157,
            child: OutlinedButton(
              onPressed: () {},
              child: SvgPicture.asset(
                'assets/images/icon/twitter.svg',
              ),
            ),
          ),
        ),
      ],
    );
  }
}
