import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/login/controllers/form_login_controller.dart';
import 'package:my_telkomsel_app/app/modules/login/controllers/login_controller.dart';
import 'package:my_telkomsel_app/theme.dart';
import 'package:loading_indicator/loading_indicator.dart';

class ButtonSave extends StatelessWidget {
  const ButtonSave({super.key});

  @override
  Widget build(BuildContext context) {
    final formController = Get.find<FormLoginController>();
    final controller = Get.find<LoginController>();

    return Obx(
      () => SizedBox(
        width: Get.width,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            disabledBackgroundColor: MyAppColor.GREY_LIGHT,
            backgroundColor: formController.isAgreementChecked.value &&
                    !controller.isSaving.value
                ? MyAppColor.RED
                : MyAppColor.GREY_LIGHT,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
              side: BorderSide(
                color: formController.isAgreementChecked.value &&
                        !controller.isSaving.value
                    ? MyAppColor.RED
                    : MyAppColor.GREY_LIGHT,
              ),
            ),
          ),
          onPressed: formController.isAgreementChecked.value &&
                  !controller.isSaving.value
              ? () => controller.saveAndNavigate(
                    formKey: formController.formLoginKey,
                  )
              : null,
          child: controller.isSaving.value
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(
                      height: 20,
                      width: 20,
                      child: LoadingIndicator(
                        indicatorType: Indicator.ballSpinFadeLoader,
                        colors: [MyAppColor.GREY_DARK],
                        strokeWidth: 2,
                        backgroundColor: MyAppColor.GREY_LIGHT,
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text(
                      'LOADING',
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: formController.isAgreementChecked.value &&
                                    !controller.isSaving.value
                                ? MyAppColor.WHITE
                                : MyAppColor.GREY_DARK,
                            letterSpacing: 1,
                          ),
                    ),
                  ],
                )
              : Text(
                  'MASUK',
                  style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        fontSize: 14,
                        fontWeight: FontWeight.w700,
                        color: formController.isAgreementChecked.value &&
                                !controller.isSaving.value
                            ? MyAppColor.WHITE
                            : MyAppColor.GREY_DARK,
                        letterSpacing: 1,
                      ),
                ),
        ),
      ),
    );
  }
}
