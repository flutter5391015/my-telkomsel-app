import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/login/controllers/form_login_controller.dart';
import 'package:my_telkomsel_app/theme.dart';

class CheckboxAgreement extends StatelessWidget {
  const CheckboxAgreement({super.key});

  @override
  Widget build(BuildContext context) {
    final formController = Get.find<FormLoginController>();
    return Obx(
      () {
        return Theme(
          data: Theme.of(context).copyWith(
            visualDensity: const VisualDensity(horizontal: -4),
          ),
          child: ListTileTheme(
            horizontalTitleGap: 0.0,
            child: CheckboxListTile(
              activeColor: MyAppColor.RED,
              contentPadding: EdgeInsets.zero,
              checkColor: MyAppColor.WHITE,
              dense: true,
              value: formController.isAgreementChecked.value,
              onChanged: (value) => {
                formController.setIsAgreement(value: value!),
              },
              controlAffinity: ListTileControlAffinity.leading,
              title: RichText(
                text: TextSpan(
                  text: 'Saya menyetujui ',
                  style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                      ),
                  children: [
                    TextSpan(
                      text: 'syarat',
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: MyAppColor.RED,
                          ),
                    ),
                    TextSpan(
                      text: ', ',
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                    ),
                    TextSpan(
                      text: 'ketentuan',
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: MyAppColor.RED,
                          ),
                    ),
                    TextSpan(
                      text: ', dan ',
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                    ),
                    TextSpan(
                      text: 'privasi ',
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: MyAppColor.RED,
                          ),
                    ),
                    TextSpan(
                      text: 'Provider kami',
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
