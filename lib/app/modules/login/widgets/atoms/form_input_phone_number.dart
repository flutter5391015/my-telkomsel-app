import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/login/controllers/form_login_controller.dart';
import 'package:my_telkomsel_app/theme.dart';

class FormInputPhoneNumber extends StatelessWidget {
  const FormInputPhoneNumber({super.key});

  @override
  Widget build(BuildContext context) {
    final formController = Get.find<FormLoginController>();
    return Obx(
      () {
        return TextFormField(
          initialValue: formController.phoneNumber.value,
          keyboardType: TextInputType.phone,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.allow(
              RegExp(r'[0-9]'),
            ),
          ],
          decoration: const InputDecoration(
            errorMaxLines: 2,
            contentPadding: EdgeInsets.only(
              bottom: 0.0,
              top: 15.0,
            ),
            prefix: Padding(
              padding: EdgeInsets.only(
                left: 10,
              ),
            ),
            hintText: 'Cth. 08129011xxxx',
            hintStyle: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              fontStyle: FontStyle.normal,
              color: MyAppColor.PLACEHOLDER,
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: MyAppColor.BORDER_TEXT_INPUT,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: MyAppColor.RED,
              ),
            ),
            errorStyle: TextStyle(
              color: MyAppColor.RED,
              overflow: TextOverflow.clip,
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: MyAppColor.RED,
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: MyAppColor.RED,
              ),
            ),
          ),
          onChanged: (newValue) => formController.setPhoneNumber(
            value: newValue,
          ),
          validator: (value) {
            if (value == null ||
                value == '' ||
                !formController.checkingPhoneFormat(
                  value: value,
                )) {
              return 'Cannot be empty and format must \'+62XXXXXXXXXXX\' or \'62XXXXXXXXXXX\' or \'0XXXXXXXXXXX\' ';
            }

            return null;
          },
        );
      },
    );
  }
}
