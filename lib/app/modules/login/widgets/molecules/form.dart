import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/login/controllers/form_login_controller.dart';
import 'package:my_telkomsel_app/app/modules/login/widgets/atoms/button_login_with_social_media.dart';
import 'package:my_telkomsel_app/app/modules/login/widgets/atoms/button_submit.dart';
import 'package:my_telkomsel_app/app/modules/login/widgets/atoms/checkbox_agreement.dart';
import 'package:my_telkomsel_app/app/modules/login/widgets/atoms/form_input_phone_number.dart';
import 'package:my_telkomsel_app/theme.dart';

class FormLogin extends StatelessWidget {
  const FormLogin({super.key});

  @override
  Widget build(BuildContext context) {
    final formController = Get.find<FormLoginController>();
    return Form(
      key: formController.formLoginKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            'Nomor HP',
            style: Theme.of(context).textTheme.labelSmall!.copyWith(
                  fontWeight: FontWeight.w700,
                  fontSize: 14,
                ),
          ),
          const SizedBox(
            height: 10,
          ),
          const FormInputPhoneNumber(),
          const SizedBox(
            height: 10,
          ),
          const CheckboxAgreement(),
          const SizedBox(
            height: 10,
          ),
          const ButtonSave(),
          const SizedBox(
            height: 10,
          ),
          Center(
            child: Text(
              'Atau masuk menggunakan',
              style: Theme.of(context).textTheme.titleMedium!.copyWith(
                    fontSize: 14,
                    color: MyAppColor.GREY_DARK,
                  ),
            ),
          ),
          const SizedBox(
            height: 24,
          ),
          const LoginWithSocialMedia(),
        ],
      ),
    );
  }
}
