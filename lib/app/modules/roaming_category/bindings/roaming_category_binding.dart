import 'package:get/get.dart';

import '../controllers/roaming_category_controller.dart';

class RoamingCategoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RoamingCategoryController>(
      () => RoamingCategoryController(),
    );
  }
}
