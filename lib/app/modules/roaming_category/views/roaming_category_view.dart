import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/roaming_category_controller.dart';

class RoamingCategoryView extends GetView<RoamingCategoryController> {
  const RoamingCategoryView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('RoamingCategoryView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'RoamingCategoryView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
