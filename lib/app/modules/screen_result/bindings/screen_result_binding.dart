import 'package:get/get.dart';

import '../controllers/screen_result_controller.dart';

class ScreenResultBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ScreenResultController>(
      () => ScreenResultController(),
    );
  }
}
