import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/screen_result_controller.dart';

class ScreenResultView extends GetView<ScreenResultController> {
  const ScreenResultView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ScreenResultView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'ScreenResultView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
