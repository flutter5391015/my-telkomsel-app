import 'package:get/get.dart';

import 'package:my_telkomsel_app/app/modules/search/controllers/telkomsel_search_controller.dart';

class SearchBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TelkomselSearchController>(
      () => TelkomselSearchController(),
    );
  }
}
