import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/search/views/search_view.dart';

class TelkomselSearchController extends GetxController {
  final previousRoute = ''.obs;
  final searchValue = ''.obs;
  final lastSearching = <Map<String, String>>[].obs;
  late TextEditingController searchInputController;

  @override
  void onInit() {
    super.onInit();
    searchInputController = TextEditingController();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
    searchInputController.dispose();
  }

  void setSearchValue({required String name}) {
    searchValue.value = name;
  }

  void showSearchView() {
    Get.bottomSheet(
      const SearchView(),
      ignoreSafeArea: false,
      isDismissible: false,
      enableDrag: false,
      isScrollControlled: true,
    );
  }

  void addLastSearching({required String value}) {
    final isExists =
        lastSearching.firstWhereOrNull((el) => el['name'] == value);

    if (isExists == null) {
      lastSearching.add({"id": DateTime.now().toString(), "name": value});
    }
  }

  void deleteLastSearching({required String id}) {
    lastSearching.removeWhere((el) => el['id'] == id);
  }

  void clearSearchValue() {
    searchValue.value = '';
    searchInputController.text = '';
  }
}
