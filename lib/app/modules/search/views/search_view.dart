import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/search/controllers/telkomsel_search_controller.dart';
import 'package:my_telkomsel_app/app/modules/search/widgets/atoms/populer_item.dart';
import 'package:my_telkomsel_app/app/modules/search/widgets/molecules/input_search.dart';
import 'package:my_telkomsel_app/theme.dart';

class SearchView extends GetView<TelkomselSearchController> {
  const SearchView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          automaticallyImplyLeading: false, // ini untuk menghapus icon back
          title: const InputSearch(),
          backgroundColor: MyAppColor.WHITE,
          actions: [
            TextButton(
              onPressed: () {
                controller.clearSearchValue();
                Get.back();
              },
              child: Text(
                'Batal',
                style: Theme.of(context).textTheme.titleLarge!.copyWith(
                      color: MyAppColor.RED,
                      fontSize: 14,
                      height: 20 / 14,
                    ),
              ),
            )
          ],
        ),
        body: GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(
            FocusNode(),
          ),
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(16, 19, 16, 12),
                child: Text(
                  'Terakhir dicari',
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge!
                      .copyWith(fontSize: 16, height: 20.35 / 16),
                ),
              ),
              SizedBox(
                height: 208,
                child: Obx(() => ListView.builder(
                      itemCount: controller.lastSearching.length,
                      itemBuilder: (context, index) {
                        return ListTile(
                          title: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                'assets/images/icon/time-backward.svg',
                                fit: BoxFit.cover,
                              ),
                              const SizedBox(
                                width: 12,
                              ),
                              Text(
                                controller.lastSearching[index]['name']
                                    .toString(),
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(
                                      color: MyAppColor.GREY_DARK,
                                      fontSize: 14,
                                      height: 20 / 14,
                                    ),
                              )
                            ],
                          ),
                          trailing: GestureDetector(
                            onTap: () => controller.deleteLastSearching(
                              id: controller.lastSearching[index]['id']
                                  .toString(),
                            ),
                            child: SvgPicture.asset(
                                'assets/images/icon/round-close.svg'),
                          ),
                        );
                      },
                    )),
              ),
              const SizedBox(
                height: 16,
              ),
              Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                ),
                width: Get.width * 0.9,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Pencarian populer',
                      style: Theme.of(context)
                          .textTheme
                          .titleLarge!
                          .copyWith(fontSize: 16, height: 20.35 / 16),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Wrap(
                      spacing: 8,
                      runSpacing: 8,
                      runAlignment: WrapAlignment.start,
                      children: [
                        PopularItem(
                          name: 'ruangguru',
                          onTap: () => {},
                        ),
                        PopularItem(
                          name: 'ketengan',
                          onTap: () => {},
                        ),
                        PopularItem(
                          name: 'Conference',
                          onTap: () => {},
                        ),
                        PopularItem(
                          name: 'Omg',
                          onTap: () => {},
                        ),
                        PopularItem(
                          name: 'ilmupedia',
                          onTap: () => {},
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
