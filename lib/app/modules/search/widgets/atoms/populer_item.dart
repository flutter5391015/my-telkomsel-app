import 'package:flutter/material.dart';
import 'package:my_telkomsel_app/theme.dart';

class PopularItem extends StatelessWidget {
  final String name;
  final void Function() onTap;
  const PopularItem({super.key, required this.name, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        outlinedButtonTheme: OutlinedButtonThemeData(
          style: OutlinedButton.styleFrom(
            splashFactory: NoSplash.splashFactory,
            foregroundColor: MyAppColor.RED,
            side: const BorderSide(
              color: MyAppColor.RED,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(
                16.0,
              ), // Menyesuaikan radius border sesuai keinginan Anda
            ),
          ),
        ),
      ),
      child: SizedBox(
        height: 32,
        child: OutlinedButton(
          onPressed: () {},
          child: Text(
            name,
            style: Theme.of(context).textTheme.titleMedium!.copyWith(
                  color: MyAppColor.RED,
                  fontSize: 12,
                  height: 16 / 12,
                ),
          ),
        ),
      ),
    );
  }
}
