import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/search/controllers/telkomsel_search_controller.dart';
import 'package:my_telkomsel_app/theme.dart';

class InputSearch extends StatelessWidget {
  const InputSearch({super.key});

  @override
  Widget build(BuildContext context) {
    final searchController = Get.find<TelkomselSearchController>();

    return Obx(
      () => TextField(
        autofocus: true,
        controller: searchController.searchInputController,
        onChanged: (value) => searchController.setSearchValue(name: value),
        onSubmitted: (value) => searchController.addLastSearching(value: value),
        cursorColor: MyAppColor.RED,
        style: const TextStyle(
          color: MyAppColor.RED,
        ),
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.only(
            bottom: 0.0,
            top: 15.0,
          ),
          border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(4),
            ),
          ),
          prefixIcon: const Icon(
            Icons.search,
          ),
          suffixIcon: searchController.searchValue.value != ''
              ? IconButton(
                  enableFeedback: false,
                  onPressed: searchController.clearSearchValue,
                  icon: SvgPicture.asset('assets/images/icon/close-circle.svg'),
                )
              : null,
          prefixIconColor: MyAppColor.PLACEHOLDER,
          hintText: 'Cari paket favorit kamu ...',
          hintStyle: const TextStyle(
            color: MyAppColor.PLACEHOLDER,
          ),
          enabledBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: MyAppColor.GREY,
            ),
          ),
          focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(
              color: MyAppColor.RED,
            ),
          ),
          fillColor: MyAppColor.GREY,
          filled: true,
        ),
      ),
    );
  }
}
