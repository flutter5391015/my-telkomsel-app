import 'package:get/get.dart';

import '../controllers/sms_category_controller.dart';

class SmsCategoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SmsCategoryController>(
      () => SmsCategoryController(),
    );
  }
}
