import 'package:get/get.dart';

class SmsCategoryController extends GetxController {
  //TODO: Implement SmsCategoryController

  final count = 0.obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void increment() => count.value++;
}
