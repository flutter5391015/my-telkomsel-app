import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/sms_category_controller.dart';

class SmsCategoryView extends GetView<SmsCategoryController> {
  const SmsCategoryView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SmsCategoryView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'SmsCategoryView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
