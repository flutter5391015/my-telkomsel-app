import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/routes/app_pages.dart';

class SplashController extends GetxController {
  @override
  void onReady() {
    Future.delayed(
      const Duration(
        seconds: 2,
      ),
      () {
        Get.offNamed(
          Routes.LOGIN,
        );
      },
    );
    super.onReady();
  }
}
