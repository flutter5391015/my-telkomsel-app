import 'package:flutter/material.dart';

import 'package:get/get.dart';

import 'package:my_telkomsel_app/app/modules/splash/controllers/splash_controller.dart';
import 'package:my_telkomsel_app/theme.dart';

class SplashView extends GetView<SplashController> {
  const SplashView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    controller.onReady();

    return Scaffold(
      backgroundColor: MyAppColor.RED,
      body: Center(
        child: SizedBox(
          height: 100,
          width: 97,
          child: Image.asset(
            'assets/images/logo.png',
            fit: BoxFit.contain,
            alignment: Alignment.center,
          ),
        ),
      ),
    );
  }
}
