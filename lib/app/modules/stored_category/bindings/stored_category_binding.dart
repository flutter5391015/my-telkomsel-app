import 'package:get/get.dart';

import '../controllers/stored_category_controller.dart';

class StoredCategoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<StoredCategoryController>(
      () => StoredCategoryController(),
    );
  }
}
