import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/stored_category_controller.dart';

class StoredCategoryView extends GetView<StoredCategoryController> {
  const StoredCategoryView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('StoredCategoryView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'StoredCategoryView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
