import 'package:get/get.dart';

import '../controllers/superior_category_controller.dart';

class SuperiorCategoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SuperiorCategoryController>(
      () => SuperiorCategoryController(),
    );
  }
}
