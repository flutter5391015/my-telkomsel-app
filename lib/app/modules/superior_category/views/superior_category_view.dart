import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/superior_category_controller.dart';

class SuperiorCategoryView extends GetView<SuperiorCategoryController> {
  const SuperiorCategoryView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('SuperiorCategoryView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'SuperiorCategoryView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
