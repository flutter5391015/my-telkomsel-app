import 'package:get/get.dart';

import '../controllers/telpon_category_controller.dart';

class TelponCategoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TelponCategoryController>(
      () => TelponCategoryController(),
    );
  }
}
