import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/telpon_category_controller.dart';

class TelponCategoryView extends GetView<TelponCategoryController> {
  const TelponCategoryView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('TelponCategoryView'),
        centerTitle: true,
      ),
      body: const Center(
        child: Text(
          'TelponCategoryView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
