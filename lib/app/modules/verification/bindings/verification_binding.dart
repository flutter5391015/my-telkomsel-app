import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/verification/controllers/form_verification_controller.dart';

import 'package:my_telkomsel_app/app/modules/verification/controllers/verification_controller.dart';

class VerificationBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FormVerificationController>(
      () => FormVerificationController(),
    );

    Get.lazyPut<VerificationController>(
      () => VerificationController(),
    );
  }
}
