import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/login/controllers/login_controller.dart';

class FormVerificationController extends GetxController {
  final uniqueCode = ''.obs;
  late TextEditingController codeController;
  final formVerificationKey = GlobalKey<FormState>();
  final loginController = Get.find<LoginController>();

  Future<String> generateRandomString() async {
    await Future.delayed(
      const Duration(
        seconds: 2,
      ),
    );
    final random = Random();
    const chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
    uniqueCode.value = List.generate(
      5,
      (index) => chars[random.nextInt(chars.length)],
    ).join();

    return uniqueCode.value;
  }

  @override
  void onInit() {
    super.onInit();
    codeController = TextEditingController();
    codeController.text = '';
  }

  @override
  void onReady() async {
    super.onReady();
    codeController.text = await generateRandomString();
  }

  @override
  void onClose() {
    super.onClose();
    codeController.dispose();
  }
}
