import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/home/controllers/home_controller.dart';
import 'package:my_telkomsel_app/app/routes/app_pages.dart';

class VerificationController extends GetxController {
  final isSaving = false.obs;
  final homeController = Get.find<HomeController>();

  Future<void> saveAndNavigate({
    required GlobalKey<FormState> formKey,
  }) async {
    final formVerification = formKey.currentState!;
    isSaving.value = true;
    if (formVerification.validate()) {
      formVerification.save();

      await Future.delayed(
        const Duration(
          seconds: 2,
        ),
      );

      homeController.setVerified(isVerified: true);

      Get.offAllNamed(Routes.HOME);
    }

    isSaving.value = false;
  }
}
