import 'package:flutter/material.dart';

import 'package:get/get.dart';

import 'package:my_telkomsel_app/app/modules/verification/controllers/verification_controller.dart';
import 'package:my_telkomsel_app/app/modules/verification/widgets/atoms/button_verification.dart';
import 'package:my_telkomsel_app/app/modules/verification/widgets/atoms/summary.dart';
import 'package:my_telkomsel_app/app/modules/verification/widgets/molecules/form.dart';
import 'package:my_telkomsel_app/app/modules/verification/widgets/molecules/image_verification.dart';
import 'package:my_telkomsel_app/theme.dart';

class VerificationView extends GetView<VerificationController> {
  const VerificationView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true, // untuk membuat title dari appbar
        title: null,
        backgroundColor: MyAppColor.WHITE,
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: ListView(
            children: [
              const Align(
                alignment: Alignment.topLeft,
                child: ImageVerification(),
              ),
              const SizedBox(
                height: 16,
              ),
              Text(
                'Masukan kode unik yang kami kirim',
                style: Theme.of(context).textTheme.titleLarge!.copyWith(
                      fontSize: 18,
                      height: 23 / 18,
                    ),
              ),
              const SizedBox(
                height: 10,
              ),
              const SummaryVerification(),
              const SizedBox(
                height: 24,
              ),
              const FormVerification(),
              const SizedBox(
                height: 10,
              ),
              RichText(
                text: TextSpan(
                  text: 'Tidak menerima SMS ?',
                  style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        fontSize: 14,
                        height: 18 / 14,
                        fontWeight: FontWeight.w400,
                      ),
                  children: [
                    TextSpan(
                      text: ' Kirim ulang',
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            fontSize: 14,
                            height: 18 / 14,
                            fontWeight: FontWeight.w700,
                            color: MyAppColor.RED,
                          ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              const ButtonVerification(),
            ],
          ),
        ),
      ),
    );
  }
}
