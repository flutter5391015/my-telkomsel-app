import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/verification/controllers/form_verification_controller.dart';
import 'package:my_telkomsel_app/app/modules/verification/controllers/verification_controller.dart';
import 'package:my_telkomsel_app/theme.dart';
import 'package:loading_indicator/loading_indicator.dart';

class ButtonVerification extends StatelessWidget {
  const ButtonVerification({super.key});

  @override
  Widget build(BuildContext context) {
    final formVerificationController = Get.find<FormVerificationController>();
    final controller = Get.find<VerificationController>();
    return Obx(
      () => SizedBox(
        width: Get.width,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            disabledBackgroundColor: MyAppColor.GREY_LIGHT,
            backgroundColor: !controller.isSaving.value &&
                    formVerificationController.uniqueCode.value != ''
                ? MyAppColor.RED
                : MyAppColor.GREY_LIGHT,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
              side: BorderSide(
                color: !controller.isSaving.value &&
                        formVerificationController.uniqueCode.value != ''
                    ? MyAppColor.RED
                    : MyAppColor.GREY_LIGHT,
              ),
            ),
          ),
          onPressed: !controller.isSaving.value &&
                  formVerificationController.uniqueCode.value != ''
              ? () => controller.saveAndNavigate(
                    formKey: formVerificationController.formVerificationKey,
                  )
              : null,
          child: controller.isSaving.value
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(
                      height: 20,
                      width: 20,
                      child: LoadingIndicator(
                        indicatorType: Indicator.ballSpinFadeLoader,
                        colors: [MyAppColor.GREY_DARK],
                        strokeWidth: 2,
                        backgroundColor: MyAppColor.GREY_LIGHT,
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text(
                      'LOADING',
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: !controller.isSaving.value
                                ? MyAppColor.WHITE
                                : MyAppColor.GREY_DARK,
                            letterSpacing: 1,
                          ),
                    ),
                  ],
                )
              : Text(
                  'VERIFY',
                  style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        fontSize: 14,
                        fontWeight: FontWeight.w700,
                        color: !controller.isSaving.value &&
                                formVerificationController.uniqueCode.value !=
                                    ''
                            ? MyAppColor.WHITE
                            : MyAppColor.GREY_DARK,
                        letterSpacing: 1,
                      ),
                ),
        ),
      ),
    );
  }
}
