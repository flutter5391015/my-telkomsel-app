import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/verification/controllers/form_verification_controller.dart';
import 'package:my_telkomsel_app/theme.dart';

class FormInputCode extends StatelessWidget {
  const FormInputCode({super.key});

  @override
  Widget build(BuildContext context) {
    final formVerificationController = Get.find<FormVerificationController>();
    return TextFormField(
      controller: formVerificationController.codeController,
      decoration: const InputDecoration(
        errorMaxLines: 2,
        contentPadding: EdgeInsets.only(
          bottom: 0.0,
          top: 15.0,
        ),
        prefix: Padding(
          padding: EdgeInsets.only(
            left: 10,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: MyAppColor.BORDER_TEXT_INPUT,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: MyAppColor.RED,
          ),
        ),
        errorStyle: TextStyle(
          color: MyAppColor.RED,
          overflow: TextOverflow.clip,
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: MyAppColor.RED,
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: MyAppColor.RED,
          ),
        ),
      ),
      validator: (value) {
        if (value == null || value == '') {
          return 'Cannot be empty';
        }

        return null;
      },
    );
  }
}
