import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/login/controllers/login_controller.dart';

class SummaryVerification extends StatelessWidget {
  const SummaryVerification({super.key});

  @override
  Widget build(BuildContext context) {
    final loginController = Get.find<LoginController>();
    return Obx(
      () => RichText(
        text: TextSpan(
          text:
              'Silahkan periksa SMS kamu dan masukan kode unik yang kami kirimkan ke ',
          style: Theme.of(context).textTheme.titleSmall!.copyWith(
                fontSize: 14,
                height: 18 / 14,
                fontWeight: FontWeight.w400,
              ),
          children: [
            TextSpan(
              text: loginController.userLogin.value.phoneNumber,
              style: Theme.of(context).textTheme.titleSmall!.copyWith(
                    fontSize: 14,
                    height: 18 / 14,
                    fontWeight: FontWeight.w700,
                  ),
            ),
          ],
        ),
      ),
    );
  }
}
