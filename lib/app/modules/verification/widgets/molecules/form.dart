import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_telkomsel_app/app/modules/verification/controllers/form_verification_controller.dart';
import 'package:my_telkomsel_app/app/modules/verification/widgets/atoms/form_input_code.dart';

class FormVerification extends StatelessWidget {
  const FormVerification({super.key});

  @override
  Widget build(BuildContext context) {
    final formVerificationController = Get.find<FormVerificationController>();
    return Form(
      key: formVerificationController.formVerificationKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            'Kode Unique',
            style: Theme.of(context).textTheme.labelSmall!.copyWith(
                  fontWeight: FontWeight.w700,
                  fontSize: 14,
                ),
          ),
          const SizedBox(
            height: 10,
          ),
          const FormInputCode(),
        ],
      ),
    );
  }
}
