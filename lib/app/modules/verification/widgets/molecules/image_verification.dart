import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ImageVerification extends StatelessWidget {
  const ImageVerification({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 133.63,
      width: 147.98,
      child: SvgPicture.asset(
        'assets/images/icon/image-verification.svg',
        fit: BoxFit.contain,
      ),
    );
  }
}
