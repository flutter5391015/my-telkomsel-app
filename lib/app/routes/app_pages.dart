import 'package:get/get.dart';

import '../modules/entertainment_category/bindings/entertainment_category_binding.dart';
import '../modules/entertainment_category/views/entertainment_category_view.dart';
import '../modules/history_category/bindings/history_category_binding.dart';
import '../modules/history_category/views/history_category_view.dart';
import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/internet_category/bindings/internet_category_binding.dart';
import '../modules/internet_category/views/internet_category_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/roaming_category/bindings/roaming_category_binding.dart';
import '../modules/roaming_category/views/roaming_category_view.dart';
import '../modules/screen_result/bindings/screen_result_binding.dart';
import '../modules/screen_result/views/screen_result_view.dart';
import '../modules/search/bindings/search_binding.dart';
import '../modules/search/views/search_view.dart';
import '../modules/sms_category/bindings/sms_category_binding.dart';
import '../modules/sms_category/views/sms_category_view.dart';
import '../modules/splash/bindings/splash_binding.dart';
import '../modules/splash/views/splash_view.dart';
import '../modules/stored_category/bindings/stored_category_binding.dart';
import '../modules/stored_category/views/stored_category_view.dart';
import '../modules/superior_category/bindings/superior_category_binding.dart';
import '../modules/superior_category/views/superior_category_view.dart';
import '../modules/telpon_category/bindings/telpon_category_binding.dart';
import '../modules/telpon_category/views/telpon_category_view.dart';
import '../modules/verification/bindings/verification_binding.dart';
import '../modules/verification/views/verification_view.dart';

// ignore_for_file: constant_identifier_names

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.SPLASH;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.SPLASH,
      page: () => const SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => const LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.VERIFICATION,
      page: () => const VerificationView(),
      binding: VerificationBinding(),
    ),
    GetPage(
      name: _Paths.INTERNET_CATEGORY,
      page: () => const InternetCategoryView(),
      binding: InternetCategoryBinding(),
    ),
    GetPage(
      name: _Paths.TELPON_CATEGORY,
      page: () => const TelponCategoryView(),
      binding: TelponCategoryBinding(),
    ),
    GetPage(
      name: _Paths.SMS_CATEGORY,
      page: () => const SmsCategoryView(),
      binding: SmsCategoryBinding(),
    ),
    GetPage(
      name: _Paths.ROAMING_CATEGORY,
      page: () => const RoamingCategoryView(),
      binding: RoamingCategoryBinding(),
    ),
    GetPage(
      name: _Paths.ENTERTAINMENT_CATEGORY,
      page: () => const EntertainmentCategoryView(),
      binding: EntertainmentCategoryBinding(),
    ),
    GetPage(
      name: _Paths.SUPERIOR_CATEGORY,
      page: () => const SuperiorCategoryView(),
      binding: SuperiorCategoryBinding(),
    ),
    GetPage(
      name: _Paths.STORED_CATEGORY,
      page: () => const StoredCategoryView(),
      binding: StoredCategoryBinding(),
    ),
    GetPage(
      name: _Paths.HISTORY_CATEGORY,
      page: () => const HistoryCategoryView(),
      binding: HistoryCategoryBinding(),
    ),
    GetPage(
      name: _Paths.SEARCH,
      page: () => const SearchView(),
      binding: SearchBinding(),
      transition: Transition.downToUp,
    ),
    GetPage(
      name: _Paths.SCREEN_RESULT,
      page: () => const ScreenResultView(),
      binding: ScreenResultBinding(),
    ),
  ];
}
