// ignore_for_file: constant_identifier_names

part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const SPLASH = _Paths.SPLASH;
  static const LOGIN = _Paths.LOGIN;
  static const VERIFICATION = _Paths.VERIFICATION;
  static const INTERNET_CATEGORY = _Paths.INTERNET_CATEGORY;
  static const TELPON_CATEGORY = _Paths.TELPON_CATEGORY;
  static const SMS_CATEGORY = _Paths.SMS_CATEGORY;
  static const ROAMING_CATEGORY = _Paths.ROAMING_CATEGORY;
  static const ENTERTAINMENT_CATEGORY = _Paths.ENTERTAINMENT_CATEGORY;
  static const SUPERIOR_CATEGORY = _Paths.SUPERIOR_CATEGORY;
  static const STORED_CATEGORY = _Paths.STORED_CATEGORY;
  static const HISTORY_CATEGORY = _Paths.HISTORY_CATEGORY;
  static const SEARCH = _Paths.SEARCH;
  static const SCREEN_RESULT = _Paths.SCREEN_RESULT;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const SPLASH = '/splash';
  static const LOGIN = '/login';
  static const VERIFICATION = '/verification';
  static const INTERNET_CATEGORY = '/internet-category';
  static const TELPON_CATEGORY = '/telpon-category';
  static const SMS_CATEGORY = '/sms-category';
  static const ROAMING_CATEGORY = '/roaming-category';
  static const ENTERTAINMENT_CATEGORY = '/entertainment-category';
  static const SUPERIOR_CATEGORY = '/superior-category';
  static const STORED_CATEGORY = '/stored-category';
  static const HISTORY_CATEGORY = '/history-category';
  static const SEARCH = '/search';
  static const SCREEN_RESULT = '/screen-result';
}
