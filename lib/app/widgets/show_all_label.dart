import 'package:flutter/material.dart';
import 'package:my_telkomsel_app/theme.dart';

class ShowAllLabel extends StatelessWidget {
  const ShowAllLabel({super.key});

  @override
  Widget build(BuildContext context) {
    return Text(
      'Lihat Semua',
      style: Theme.of(context).textTheme.titleLarge!.copyWith(
            fontSize: 14,
            fontWeight: FontWeight.w500,
            height: 17.81 / 14,
            color: MyAppColor.RED,
          ),
    );
  }
}
