import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:my_telkomsel_app/theme.dart';

import 'app/routes/app_pages.dart';

void main() async {
  WidgetsFlutterBinding
      .ensureInitialized(); // ini digunakan untuk memastikan bahwa binding sudah diinisialisasi
  runApp(
    GetMaterialApp(
      defaultTransition: Transition.native,
      theme: appTheme,
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
    ),
  );
}
