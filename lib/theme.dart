// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

abstract class MyAppColor {
  static const Color GREY = Color(0xFFF1F2F6);
  static const Color WHITE = Color(0xFFFFFFFF);
  static const Color BLACK = Color(0xFF1E272E);
  static const Color GREY_DARK = Color(0xFF747D8C);
  static const Color YELLOW = Color(0xFFF7B731);
  static const Color RED = Color(0xFFEC2028);
  static const Color GREY_LIGHT = Color(0xFFE4E5EA);
  static const Color BORDER_TEXT_INPUT = Color(0xFFCED6E0);
  static const Color PLACEHOLDER = Color(0xFFA4B0BE);
  static const LinearGradient LINER_RED = LinearGradient(
    colors: [
      Color(0xFFE52D27),
      Color(0xFFB31217),
    ],
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
  );
}

const myAppColorScheme = ColorScheme(
  brightness: Brightness.light,
  background: MyAppColor.WHITE, //untuk warna dasar aplikasi
  onBackground: Colors.red, // untuk tombol back di appbar
  outline: MyAppColor.BORDER_TEXT_INPUT, // untuk outline
  surface: MyAppColor.YELLOW, // untuk button
  onSurface: MyAppColor.BLACK, // untuk tombol back di appbar
  primary: Colors.white,
  onPrimary: Colors.orange,
  error: Colors.green,
  onError: Colors.white,
  secondary: Colors.indigo,
  onSecondary: Colors.grey,
);

final ThemeData appTheme = ThemeData(
  useMaterial3: true,
  colorScheme: myAppColorScheme,
  textTheme: GoogleFonts.openSansTextTheme().copyWith(
    titleSmall: GoogleFonts.plusJakartaSans(
      fontWeight: FontWeight.normal,
      color: MyAppColor.BLACK,
    ),
    titleMedium: GoogleFonts.plusJakartaSans(
      fontWeight: FontWeight.w500,
      color: MyAppColor.BLACK,
    ),
    titleLarge: GoogleFonts.plusJakartaSans(
      fontWeight: FontWeight.w700,
      color: MyAppColor.BLACK,
    ),
  ),
);
